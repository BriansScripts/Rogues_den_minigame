package impl;

import data.Location;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class downdownpath extends Task {
    @Override
    public boolean validate(){
        //start of the maze until before the wall of pendulums on the far southwest room
        if ((Location.downdownArea.contains(Players.getLocal()))){
            Log.info("Outside lower path");
            return true;
        }else{ return false; }
    }

    @Override
    public int execute(){
        switch (getobstacle()){
            case 1:
                SceneObject bars = SceneObjects.getNearest("Contortion Bars");
                if (bars != null){
                    if (bars.interact("Enter")){
                        Time.sleepUntil(()-> getobstacle() == 2,()->(Players.getLocal().isMoving() || Players.getLocal().isAnimating()), 1500);
                    }
                }
                break;
            case 2:
                if (Movement.walkTo(new Position(3039,4999,1))){
                    Time.sleepUntil(()-> getobstacle() == 3,()->(Players.getLocal().isMoving() || Players.getLocal().isAnimating()), 1500);
                }
                break;
            case 3:
                Position walk1 = new Position(3033,5002,1);
                if (Movement.walkTo(walk1)){
                    if (Time.sleepUntil(()-> walk1.distance() < 1,()->(Players.getLocal().isMoving()), 1500)){
                        if (walk1.distance() < 1){
                            SceneObject grill = SceneObjects.getNearest("Grill");
                            if (grill != null){
                                if (grill.interact("Open")){
                                    Time.sleepUntil(()-> getobstacle() == 4,()->(Players.getLocal().isMoving() || Players.getLocal().isAnimating()), 1500);
                                }
                            }
                        }
                    }
                }
                break;
            case 4:
                Position walk2 = new Position(3011,5005,1);
                if (Movement.walkTo(walk2)){
                    if (Time.sleepUntil(()-> walk2.distance() < 1,()->(Players.getLocal().isMoving()), 1500)){
                        if (Players.getLocal().getX() < 3012){
                            SceneObject ledge = SceneObjects.getNearest("Ledge");
                            if (ledge != null){
                                if (ledge.interact("Climb")){
                                    Time.sleepUntil(()-> getobstacle() == 5,()->(Players.getLocal().isMoving() || Players.getLocal().isAnimating()), 1500);
                                }
                            }
                        }
                    }
                }
                break;
            case 5:
                Position walk3 = new Position(2969,5017,1);
                if (Movement.walkTo(walk3)){
                    Time.sleepUntil(()-> getobstacle() == 6,()->(Players.getLocal().isMoving()|| Players.getLocal().isAnimating()), 1500);
                }
                break;
        }
        return 100;
    }

    private int getobstacle(){
        int mylocation = Players.getLocal().getX();
        if (mylocation > 3048){
            //Contortion Bars
            return 1;
        }else if (mylocation > 3039){
            //Pendulem
            return 2;
        }else if (mylocation > 3023){
            //Grill
            return 3;
        }else if (mylocation > 2988){
            //Ledge
            return 4;
        }else if (mylocation > 2967){
            //Saw
            return 5;
        }else {
            return 6;
        }
    }

}

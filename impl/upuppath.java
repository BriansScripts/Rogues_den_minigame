package impl;

import data.Location;
import org.rspeer.runetek.adapter.scene.Pickable;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Pickables;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class upuppath extends Task {
    @Override
    public boolean validate(){
        //start northwest room and goes until the passage after the tile is entered
        if ((Location.upupArea.contains(Players.getLocal())) && !Location.leftleftArea.contains(Players.getLocal())){
            Log.info("Outside upper path");
            return true;
        }else{ return false; }
    }

    @Override
    public int execute(){
        switch (getobstacle()){
            case 1:
                Position walk1 = new Position(2972,5098,1);
                if (Movement.walkTo(walk1)){
                    Time.sleepUntil(()-> walk1.distance() < 1,()->Players.getLocal().isMoving(),1500);
                }
                break;
            case 2:
                if (Players.getLocal().getY() > 5097){
                    SceneObject passageway = SceneObjects.getNearest("Passageway");
                    if (passageway != null) {
                        if (passageway.interact("Enter")) {
                            Time.sleepUntil(() -> Players.getLocal().getY() < 5097, () -> (Players.getLocal().isMoving() || Players.getLocal().isAnimating()), 1500);
                        }
                    }
                }else if (Players.getLocal().getY() == 5094){
                    SceneObject grill = SceneObjects.getNearest("Grill");
                    if (grill != null) {
                        if (grill.interact("Open")) {
                            Time.sleepUntil(() -> getobstacle() == 3, () -> (Players.getLocal().isMoving() || Players.getLocal().isAnimating()), 1500);
                        }
                    }
                }
                break;
            case 3:
                SceneObject ledge = SceneObjects.getNearest("Ledge");
                if (ledge != null) {
                    if (ledge.interact("Climb")) {
                        Time.sleepUntil(() -> getobstacle() == 4, () -> (Players.getLocal().isMoving() || Players.getLocal().isAnimating()), 1500);
                    }
                }
                break;
            case 4:
                SceneObject wall = SceneObjects.getNearest("Wall");
                if (wall != null) {
                    if (wall.interact("Search")) {
                        Time.sleepUntil(() -> getobstacle() == 5, () -> (Players.getLocal().isMoving() || Players.getLocal().isAnimating()), 1500);
                    }
                }
                break;
            case 5:
                Position walk2 = new Position(3008,5088,1);
                if (Movement.walkTo(walk2)){
                    Time.sleepUntil(()-> walk2.distance() < 3,()->Players.getLocal().isMoving(),1500);
                }
                break;
            case 6:
                if (!Interfaces.isOpen(293)){
                    if (Inventory.contains("Tile")){
                        SceneObject door = SceneObjects.getNearest("Door");
                        if (door != null) {
                            if (door.interact("Open")) {
                                Time.sleepUntil(() -> Interfaces.isOpen(293), () -> (Players.getLocal().isMoving() || Players.getLocal().isAnimating()), 1500);
                            }
                        }
                    }else{
                         Pickable tile = Pickables.getFirstAt(new Position(3018,5080,1));
                         if (tile != null) {
                             if (tile.interact("Take")) {
                                 Time.sleepUntil(() -> Inventory.contains("Tile"), () -> (Players.getLocal().isMoving()), 1500);
                             }
                         }
                    }
                }else{
                    if (Interfaces.getComponent(293,3).interact("Ok")){
                        Time.sleepUntil(() -> getobstacle() == 7, () -> (Players.getLocal().isMoving() || Players.getLocal().isAnimating()), 1500);

                    }
                }
                break;
        }
        return 100;
    }

    private int getobstacle(){
        int mylocation = Players.getLocal().getX();
        if (mylocation < 2972){
            //walk to passageway
            return 1;
        }else if (mylocation == 2972 && Players.getLocal().getY() > 5093){
            //passageway and grill
            return 2;
        }
        else if (mylocation < 2991){
            //climb ledge
            return 3;
        }else if (mylocation < 2993){
            //search wall
            return 4;
        }else if (mylocation < 3006){
            //walk once, tile 3008,5088
            return 5;
        } else if (mylocation < 3024) {
            //pickup tile and use it on the door
            return 6;
        } else {
            return 7;
        }
    }
}

package impl;

import data.Location;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class leftleftpath extends Task {
    @Override
    public boolean validate(){
        //start before the wall of pendulems and goes until the northwest room (exclusive)
        if ((Location.leftleftArea.contains(Players.getLocal()))){
            Log.info("Outside left path");
            return true;
        }else{ return false; }
    }

    @Override
    public int execute(){
        switch (getobstacle()){
            case 1:
                if (Movement.walkTo(new Position(2958,5028,1))){
                    Time.sleepUntil(()-> getobstacle() == 2,()->(Players.getLocal().isMoving() || Players.getLocal().isAnimating()), 1500);
                }
                break;
            case 2:
                SceneObject ledge = SceneObjects.getNearest("Ledge");
                if (ledge != null){
                    if (ledge.interact("Climb")){
                        Time.sleepUntil(()-> getobstacle() == 3,()->(Players.getLocal().isMoving() || Players.getLocal().isAnimating()), 1500);
                    }
                }
                break;
            case 3:
                SceneObject floor = SceneObjects.getNearest("Floor");
                if (floor != null){
                    if (floor.interact("Search")){
                        Time.sleepUntil(()-> getobstacle() == 4,()->(Players.getLocal().isMoving() || Players.getLocal().isAnimating()), 1500);
                    }
                }
                break;
            case 4:
                Position walk1 = new Position(2957,5068,1);
                if (walk1.distance() < 3) {
                    SceneObject passageway = SceneObjects.getNearest("Passageway");
                    if (passageway != null) {
                        if (passageway.interact("Enter")) {
                            Time.sleepUntil(() -> getobstacle() == 5, () -> (Players.getLocal().isMoving() || Players.getLocal().isAnimating()), 1500);
                        }
                    }
                }else{
                    if (Movement.walkTo(walk1)){
                        Time.sleepUntil(()-> walk1.distance() < 3, () -> Players.getLocal().isMoving(),1500);
                    }
                }
                break;
            case 5:
                if (Movement.walkTo(new Position(2957,5076,1))){
                    Time.sleepUntil(()-> getobstacle() == 6,()->(Players.getLocal().isMoving() || Players.getLocal().isAnimating()), 1500);
                }
                break;
            case 6:
                Position walk2 = new Position(2955,5092,1);
                if (walk2.distance() < 3) {
                    SceneObject passageway2 = SceneObjects.getNearest("Passageway");
                    if (passageway2 != null) {
                        if (passageway2.interact("Enter")) {
                            Time.sleepUntil(() -> getobstacle() == 7, () -> (Players.getLocal().isMoving() || Players.getLocal().isAnimating()), 1500);
                        }
                    }
                }else{
                    if (Movement.walkTo(walk2)){
                        Time.sleepUntil(()-> walk2.distance() < 3, () -> Players.getLocal().isMoving(),1500);
                    }
                }
                break;
        }
        return 100;
    }

    private int getobstacle() {
        int mylocation = Players.getLocal().getY();
        if (mylocation < 5028){
            //Pendulem wall
            return 1;
        }else if (mylocation < 5035){
            //Log cross
            return 2;
        }else if (mylocation < 5051){
            //Search floor
            return 3;
        }else if (mylocation < 5072){
            //Enter passageway
            return 4;
        }else if (mylocation < 5076){
            //Cross saw
            return 5;
        }else if (mylocation < 5098){
            return 6;
        }else {
            return 7;
        }
    }
}

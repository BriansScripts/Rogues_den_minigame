package impl;

import data.Location;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;



public class mazepath extends Task {

    Position doors[] = {new Position(3030,5079,1),new Position(3032,5078,1),
    new Position(3036,5076,1),new Position(3039,5079,1),
    new Position(3042,5076,1),new Position(3044,5069,1),
    new Position(3041,5068,1),new Position(3040,5070,1),
    new Position(3038,5069,1)};

    @Override
    public boolean validate(){
        if ((Location.mazeArea.contains(Players.getLocal()))){
            Log.info("Maze area");
            return true;
        }else{ return false; }
    }

    @Override
    public int execute(){
        for (Position pos : doors){
            SceneObject door = SceneObjects.getFirstAt(pos);
            if (door != null){
                if (door.interact("Open")){
                    Time.sleepUntil(()-> Players.getLocal().isMoving(),600);
                    Time.sleepUntil(()-> !Players.getLocal().isMoving(),5500);
                    Time.sleep(1000,1100);
                }
            }
        }

        return 100;
    }



}

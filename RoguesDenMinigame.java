import impl.*;
import org.rspeer.script.Script;
import org.rspeer.script.ScriptMeta;
import org.rspeer.script.ScriptCategory;
import org.rspeer.script.task.Task;
import org.rspeer.script.task.TaskScript;
import org.rspeer.ui.Log;

@ScriptMeta(name = "Rogues Den Minigame",  desc = "Gets the outfit", developer = "Brian")
public class RoguesDenMinigame extends TaskScript {

    private static final Task[] TASKS = {new downdownpath(), new outside(), new leftleftpath(), new upuppath(),new mazepath()};

    @Override
    public void onStart() {
        Log.info("Starting script");
        submit(TASKS);//When the script is first started the segment of code in this method will be ran once.
    }

    @Override
    public void onStop() {
        //When the script is stopped the segment of code in this method will be ran once.
    }
}
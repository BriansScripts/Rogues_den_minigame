package data;

import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.movement.position.Position;

public enum Location {

    ;
    public static final Area bankArea = Area.rectangular(3038,4961,3065,4991,1);
    public static final Area downdownArea = Area.rectangular(2969,4992,3061,5019,1);
    public static final Area leftleftArea = Area.rectangular(2945,4992,2967,5097,1);
    public static final Area upupArea = Area.rectangular(2945,5080,3023,5111,1);
    public static final Area mazeArea = Area.rectangular(3024,5067,3044,5085,1);
    Position endmaze = new Position(3038,5067,1);
    public static final Area rightrightArea = Area.rectangular(3033,5030,3055,5066,1);
    public static final Area centerdownArea = Area.rectangular(3033,5030,3055,5066,1);
}
